require.config({
    baseUrl: "./js",
    paths: {
        jquery: "../node_modules/jquery/dist/jquery.min"
    }
});

require([
    "jquery",
    "canvas_sparkline",
    "svg_sparkline"
], function(
    $,
    CanvasSparkline,
    SVGSparkline
) {
    "use strict";

    $(function() {
        var body = document.getElementsByTagName("body")[0],
            canvasDiv,
            svgDiv;


        //canvasDiv = document.createElement("div");
        //CanvasSparkline.run(canvasDiv);
        //body.appendChild(canvasDiv);

        svgDiv = document.createElement("div");
        SVGSparkline.run(svgDiv);
        body.appendChild(svgDiv);
    });
});

