RJS=node_modules/.bin/r.js

all: $(RJS)

clean:
	rm -rf node_modules

$(RJS): package.json
	npm install
