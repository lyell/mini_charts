define(["jquery", "data", "linear_scale"], function($, Data, LinearScale) {
    "use strict";

    function run(parent) {
        var canvas,
            ctx,
            count = 100,
            width = 100,
            height = 25,
            points = [],
            point,
            column = 1,
            prices = Data.INDUPrices,
            min = prices.reduce(function(prev, curr) { return prev[column] < curr[column] ? prev : curr; })[column],
            max = prices.reduce(function(prev, curr) { return prev[column] > curr[column] ? prev : curr; })[column],
            xScale = new LinearScale().domain([0, prices.length - 1]).range([0, width]),
            yScale = new LinearScale().domain([min, max]).range([height, 0]),
            i,
            j,
            length;

        prices.map(function(e, i) {
            points.push({
                x: Math.round(xScale.map(i)),
                y: Math.round(yScale.map(e[column]))
            });
        });

        for (i = 0; i < count; ++i) {
            canvas = document.createElement("canvas");
            canvas.setAttribute("width", width);
            canvas.setAttribute("height", height);

            ctx = canvas.getContext("2d");

            ctx.beginPath();

            point = points[0];
            ctx.moveTo(point.x, point.y);

            for (j = 1, length = points.length; j < length; ++j) {
                point = points[j];
                ctx.lineTo(point.x, point.y);
            }

            ctx.stroke();

            parent.appendChild(canvas);
        }

    }

    var module = {
        run: function(parent) {
            run(parent);
        }
    };

    return module;
});
