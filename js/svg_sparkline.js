define(["jquery", "data", "linear_scale"], function($, Data, LinearScale) {
    "use strict";

    function run(parent) {
        var svg,
            path,
            pathStr = "",
            count = 100,
            width = 100,
            height = 25,
            points = [],
            point,
            column = 1,
            prices = Data.INDUPrices,
            min = prices.reduce(function(prev, curr) { return prev[column] < curr[column] ? prev : curr; })[column],
            max = prices.reduce(function(prev, curr) { return prev[column] > curr[column] ? prev : curr; })[column],
            xScale = new LinearScale().domain([0, prices.length - 1]).range([0, width]),
            yScale = new LinearScale().domain([min, max]).range([height, 0]),
            i,
            j,
            length;

        prices.map(function(e, i) {
            points.push({
                x: Math.round(xScale.map(i)),
                y: Math.round(yScale.map(e[column]))
            });
        });

        for (i = 0; i < count; ++i) {
            svg = document.createElementNS("http://www.w3.org/2000/svg", "svg");
            svg.setAttribute("width", width);
            svg.setAttribute("height", height);

            point = points[0];
            pathStr = "M" + point.x +  " " + point.y;

            for (j = 1, length = points.length; j < length; ++j) {
                point = points[j];
                pathStr += " L" + point.x + " " + point.y;
            }

            path = document.createElementNS("http://www.w3.org/2000/svg", "path");
            path.setAttribute("d", pathStr);
            path.setAttribute("stroke", "black");
            path.setAttribute("fill", "none");

            svg.appendChild(path);
            parent.appendChild(svg);
        }

    }

    var module = {
        run: function(parent) {
            run(parent);
        }
    };

    return module;
});
