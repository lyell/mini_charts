define([], function() {
    "use strict";

    function LinearScale() {
        var a = 1,
            b = 0,
            domain = [0, 1],
            range = [0, 1];

        function updateAB() {
            a = (range[1] - range[0]) / (domain[1] - domain[0]);
            b = range[0] - (a * domain[0]);
        }

        this.domain = function(value) {
            domain = value;
            updateAB();
            return this;
        };

        this.range = function(value) {
            range = value;
            updateAB();
            return this;
        };

        this.map = function(x) {
            return a * x + b;
        };
    }

    return LinearScale;
});
