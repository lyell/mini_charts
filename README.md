# Mini Charts

This project is intended to compare the performance characteristics of different
rendering technologies when drawing lots of little charts on a web page. In
particular, it is meant to compare the use of Canvas, SVG and WebGL for rendering.

* Source: [https://bitbucket.org/lyell/mini_charts](https://bitbucket.org/lyell/mini_charts)
* Twitter: [@lyellhaynes](https://twitter.com/lyellhaynes)


## Quick start

From the root directory, run

	$ make

This will install the node module dependencies used in this project. After that
command has completed, you can open [index.html](index.html) in a browser to
view the tests.
